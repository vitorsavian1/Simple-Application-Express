FROM node:18-alpine3.16

WORKDIR src

COPY . .

EXPOSE 3000

RUN npm install

CMD ["npm", "start"]
